import React, { useEffect, useState, useRef, useCallback } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { addFilter } from '@wordpress/hooks';
import apiFetch from '@wordpress/api-fetch';
import { addQueryArgs } from '@wordpress/url';
import { useDebounce } from '@uidotdev/usehooks';
import { __ } from '@wordpress/i18n';
import classnames from 'classnames';

export const redIcon = L.icon({
	iconUrl: 'https://api.mapy.cz/img/api/marker/drop-red.png',
	iconSize: [22, 31],
	iconAnchor: [11, 31],
});

export const blueIcon = L.icon({
	iconUrl: 'https://api.mapy.cz/img/api/marker/drop-blue.png',
	iconSize: [22, 31],
	iconAnchor: [11, 31],
});

export const yellowIcon = L.icon({
	iconUrl: 'https://api.mapy.cz/img/api/marker/drop-yellow.png',
	iconSize: [22, 31],
	iconAnchor: [11, 31],
});

export function get (path, data = {}, args = {}) {
	if (path.match(/^https?:\/\//)) {
		return apiFetch({ url: addQueryArgs(path, data), ...args });
	} else {
		return apiFetch({ path: addQueryArgs(path, data), ...args });
	}
}

export function normalizeCssLength(value, defaultValue) {
	if (typeof value === "number") {
		return `${value}px`;
	}

	if (typeof value === "string") {
		const trimmedValue = value.trim();

		if (/^\d+(\.\d+)?$/.test(trimmedValue)) {
			return `${trimmedValue}px`;
		}

		const cssLengthRegex = /^(auto|inherit|initial|unset|0|(\d+(\.\d+)?(px|em|rem|vw|vh|vmin|vmax|%|ch|ex|cm|mm|in|pt|pc))|calc\(([^()]*\([^()]*\)|[^()]+)+\))$/;

		if (cssLengthRegex.test(trimmedValue)) {
			return trimmedValue;
		}
	}

	return defaultValue;
}

export function MapContainer ({
	map,
	setMap,
	center = { lat: 0, lng: 0 },
	zoom = 13,
	mapset = 'basic',
	onZoomEnd,
	onMoveEnd,
	className,
	style,
	scrollWheelZoom,
	markers,
}) {
	const mapRef = useRef();
	const apiKey = window.wpify_mapy_cz.api_key || '';

	useEffect(() => {
		if (mapRef.current) {
			try {
				const map = new L.Map(mapRef.current, {
					center: new L.LatLng(center.lat || 0, center.lng || 0),
					zoom,
					scrollWheelZoom,
				});

				const LogoControl = L.Control.extend({
					options: { position: 'bottomleft' },
					onAdd: () => {
						const container = L.DomUtil.create('div');
						const link = L.DomUtil.create('a', '', container);
						link.setAttribute('href', 'http://mapy.cz/');
						link.setAttribute('target', '_blank');
						link.setAttribute('rel', 'noreferrer noopenner');
						link.innerHTML = '<img src="https://api.mapy.cz/img/api/logo.svg" alt="Seznam.cz a.s." />';
						L.DomEvent.disableClickPropagation(link);
						return container;
					},
				});

				new LogoControl().addTo(map);

				L.tileLayer(`https://api.mapy.cz/v1/maptiles/${mapset}/256/{z}/{x}/{y}?apikey=${apiKey}`, {
					minZoom: 0,
					maxZoom: 19,
					attribution: '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
				}).addTo(map);

				setMap(map);
			} catch (error) {
				console.error(error);
			}
		}

		return () => {
			map && map.remove();
			setMap(null);
		};
	}, [mapset, apiKey, scrollWheelZoom, setMap]);

	useEffect(() => {
		const timeout = window.setTimeout(function () {
			if (map) {
				map.invalidateSize(true);
			}
		}, 100);

		return () => {
			window.clearTimeout(timeout);
		}
	}, [map]);
	useEffect(() => {
		if (map && onZoomEnd) {
			map.on('zoomend', onZoomEnd);
		}
		return () => {
			if (map && onZoomEnd) {
				map.off('zoomend', onZoomEnd);
			}
		};
	}, [map, onMoveEnd, onMoveEnd]);

	useEffect(() => {
		if (map && onMoveEnd) {
			map.on('moveend', onMoveEnd);
		}
		return () => {
			if (map && onMoveEnd) {
				map.off('moveend', onMoveEnd);
			}
		};
	}, [map, onMoveEnd, onMoveEnd]);

	useEffect(() => {
		if (map && Array.isArray(markers) && markers.length > 0) {
			markers.forEach(marker => {
				try {
					const markerInstance = L.marker([marker.latitude, marker.longitude], { icon: redIcon, draggable: marker.draggable });
					if (marker.card) {
						markerInstance.bindPopup(marker.card.header + marker.card.body + marker.card.footer);
					}
					map.addLayer(markerInstance);

					if (marker.onDragEnd) {
						markerInstance.on('dragend', marker.onDragEnd);
					}
				} catch (error) {
					console.error(error);
				}
			});
		}

		return () => {
			if (map) {
				map.eachLayer(layer => {
					if (layer instanceof L.Marker) {
						layer.remove();
					}
				});
			}
		};
	}, [markers, map]);

	return (
		<div ref={mapRef} className={className} style={style}/>
	);
}

const useMapyCzSuggestions = ({ query, apiKey, limit = 10, lang = 'cs' }) => {
	const [suggestions, setSuggestions] = useState({ items: [] });
	const deboundedQuery = useDebounce(query, 300);

	useEffect(() => {
		const controller = new AbortController();
		let finished = false;

		if (query && apiKey) {
			get(
				'https://api.mapy.cz/v1/suggest',
				{ limit, query: deboundedQuery, apiKey, lang },
				{ signal: controller.signal },
			).then(data => {
				setSuggestions(data);
			}).finally(() => {
				finished = true;
			});
		}

		return () => {
			if (!finished) {
				controller.abort();
			}
		};
	}, [deboundedQuery, apiKey]);

	return { suggestions };
};

export function AutoComplete ({ onSuggest, lang, className }) {
	const apiKey = window.wpify_mapy_cz.api_key || '';
	const rootRef = useRef();
	const [query, setQuery] = useState('');
	const [active, setActive] = useState(null);
	const { suggestions } = useMapyCzSuggestions({
		query,
		apiKey,
		lang,
	});

	const handleTermChange = useCallback((event) => {
		setQuery(event.target.value);
	}, []);

	const handleMouseOver = useCallback((index) => {
		setActive(index);
	}, []);

	const handleSelect = useCallback((index) => {
		if (suggestions.items[index]) {
			setActive(null);
			setQuery(suggestions.items[index].name);

			if (typeof onSuggest === 'function') {
				onSuggest(suggestions.items[index]);
			}
		}
	}, [suggestions.items, onSuggest]);

	const length = suggestions.items.length;

	const handleKeyDown = useCallback((event) => {
		if (event.key === 'ArrowUp' && active > 0) {
			setActive((active + length - 1) % length);
		} else if (event.key === 'ArrowDown' && active < length - 1) {
			setActive((active + length + 1) % length);
		} else if (event.key === 'Enter' && active !== null) {
			event.stopPropagation();
			event.preventDefault();
			handleSelect(active);
			setActive(null);
		}
	}, [active, handleSelect, length]);

	const handleFocus = useCallback(() => {
		setActive(0);
	}, []);

	return (
		<div className="wpifycf-field-mapycz__autocomplete" ref={rootRef}>
			<input
				value={query}
				onChange={handleTermChange}
				className={classnames('regular-text', className)}
				style={{ width: '100%' }}
				onKeyDown={handleKeyDown}
				onFocus={handleFocus}
				onMouseOver={() => setActive(0)}
				placeholder={__('Type to search...', 'wpify-mapy-cz')}
			/>
			{active !== null && suggestions.items.length > 0 && (
				<div className="wpifycf-field-mapycz__suggestions">
					{suggestions.items.map((suggestion, index) => (
						<button
							type="button"
							key={index}
							onClick={() => handleSelect(index)}
							onMouseOver={() => handleMouseOver(index)}
							onMouseOut={() => setActive(null)}
							className={index === active ? 'active' : ''}
						>
							<strong>
								{suggestion.name}
							</strong>
							<br/>
							<small>
								{suggestion.location}
							</small>
						</button>
					))}
					<div className="wpifycf-field-mapycz__suggestions-attribution">
						{__('Powered by', 'wpify-custom-fields')}
						<a href="https://api.mapy.cz/" target="_blank" rel="noreferrer noopenner">
							<img src="https://api.mapy.cz/img/api/logo-small.svg" width={50} alt="Mapy.cz"/>
						</a>
					</div>
				</div>
			)}
		</div>
	);
}

addFilter('wpify_mapy_cz_component_autocomplete', 'wpify-mapy-cz', () => AutoComplete);
addFilter('wpify_mapy_cz_default_icon', 'wpify-mapy-cz', () => redIcon);
