import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Select from 'react-select';
import { addFilter, applyFilters } from '@wordpress/hooks';
import { __ } from '@wordpress/i18n';
import apiFetch from '@wordpress/api-fetch';
import { TextControl } from '@wordpress/components';
import L from 'leaflet';
import { MapContainer, normalizeCssLength } from './components/MapyCz';

const MapField = ({ value = {}, onChange }) => {
	const [mapycz, setMapycz] = useState();
	const [allMarkers, setAllMarkers] = useState();

	const layer_types = {
		basic: __('Basic map', 'wpify-mapy-cz'),
		outdoor: __('Outdoor map', 'wpify-mapy-cz'),
		winter: __('Winter map', 'wpify-mapy-cz'),
		aerial: __('Aerial map', 'wpify-mapy-cz'),
	};

	useEffect(() => {
		if (value.layer_type === 'DEF_OPHOTO') {
			onChange({ ...value, layer_type: 'aerial' });
		} else if (value.layer_type === 'DEF_HYBRID') {
			onChange({ ...value, layer_type: 'outdoor' });
		} else if (value.layer_type === 'DEF_TURIST') {
			onChange({ ...value, layer_type: 'outdoor' });
		} else if (value.layer_type === 'DEF_TURIST_WINTER') {
			onChange({ ...value, layer_type: 'winter' });
		} else if (value.layer_type === 'DEF_BASE') {
			onChange({ ...value, layer_type: 'basic' });
		} else if (!value.layer_type) {
			onChange({ ...value, layer_type: 'basic' });
		}
	}, [value, onChange]);

	const {
		auto_center_zoom = false,
		description = '',
		width = '100%',
		height = '400px',
		latitude = 0,
		longitude = 0,
		show_info_window = false,
		zoom = 18,
		markers = [],
		layer_type = 'DEF_BASE',
		disable_scroll = false,
	} = value;

	useEffect(() => {
		let nextValue = value;
		const nextWidth = normalizeCssLength(width, '100%');
		const nextHeight = normalizeCssLength(height, '400px');

		if (width !== nextWidth && !currentField) {
			nextValue = { ...nextValue, width: nextWidth };
		}

		if (height !== nextHeight && !currentField) {
			nextValue = { ...nextValue, height: nextHeight };
		}

		if (nextValue !== value) {
			onChange(nextValue);
		}
	}, [value, onChange, currentField]);

	const [currentField, setCurrentField] = useState(null);

	const handleSizeBlur = useCallback(() => {
		const nextWidth = normalizeCssLength(width, '100%');
		const nextHeight = normalizeCssLength(height, '400px');

		if (width !== nextWidth && currentField === 'width') {
			onChange({ ...value, width: nextWidth });
		}

		if (height !== nextHeight && currentField === 'height') {
			onChange({ ...value, height: nextHeight });
		}

		setCurrentField(null);
	}, [value, onChange, width, height, currentField, setCurrentField]);

	const handleMoveEnd = useCallback((event) => {
		const center = event.target.getCenter();
		onChange({ ...value, latitude: center.lat, longitude: center.lng, zoom: event.target.getZoom() });
	}, [value, onChange]);

	const markersOptions = useMemo(() => allMarkers?.map((marker) => ({
		value: marker.id,
		label: marker.title,
	})), [allMarkers]);

	const selectedMarkers = useMemo(() => markersOptions?.filter((option) => markers?.includes(option.value)), [markersOptions, markers]);

	const shownMarkers = useMemo(() => markers.map(markerId => allMarkers?.find(marker => marker.id === markerId)).filter(Boolean) || [], [markers, allMarkers]);

	useEffect(() => {
		apiFetch({ path: wpify_mapy_cz.markers_api, method: 'GET' }).then(setAllMarkers);
	}, []);

	useEffect(() => {
		if (mapycz && auto_center_zoom && shownMarkers.length > 0) {
			const coordinates = shownMarkers.map(marker => [marker.latitude, marker.longitude]);
			const bounds = L.latLngBounds(coordinates);

			try {
				mapycz.fitBounds(bounds, { padding: [50, 50] });
			} catch (error) {
				console.error(error, bounds);
			}
		}
	}, [mapycz, auto_center_zoom, shownMarkers]);

	const handleLayerTypeChange = (type) => onChange({ ...value, layer_type: type.value });

	const handleDescriptionChange = (event) => onChange({ ...value, description: event.target.value });

	const handleMarkersChange = (maybeMarkers = []) => {
		onChange({
			...value,
			markers: Array.isArray(maybeMarkers) ? maybeMarkers.map(marker => marker.value) : [],
		});
	};

	const handleCenterZoomByMarkers = (event) => onChange({ ...value, auto_center_zoom: event.target.checked });

	const handleShowInfoWindow = (event) => onChange({ ...value, show_info_window: event.target.checked });

	const handleWidthChange = (width) => onChange({ ...value, width });

	const handleHeightChange = (height) => onChange({ ...value, height });

	const handleDisableScroll = (event) => onChange({ ...value, disable_scroll: event.target.checked });

	return (
		<React.Fragment>
			<MapContainer
				style={{ width, height, marginBottom: '1rem' }}
				className="mapycz"
				map={mapycz}
				setMap={setMapycz}
				center={{ lat: latitude, lng: longitude }}
				zoom={zoom}
				mapset={layer_type}
				scrollWheelZoom={!disable_scroll}
				onMoveEnd={handleMoveEnd}
				markers={shownMarkers}
			/>
			<div style={{ marginBottom: '1rem' }}>
				<label>
					<strong>
						{__('Description:', 'wpify-mapy-cz')}
					</strong>
					<br/>
					<textarea
						value={description}
						onChange={handleDescriptionChange}
						style={{ width: '100%' }}
						rows={5}
					/>
				</label>
			</div>
			<label style={{ display: 'block', marginTop: '10px' }}>
				<div style={{ marginBottom: '10px' }}>
					<strong>{__('Display type:', 'wpify-mapy-cz')}</strong>
				</div>
				<Select
					value={{
						value: layer_type,
						label: layer_types[layer_type],
					}}
					options={Object.keys(layer_types).map(value => ({
						value,
						label: layer_types[value],
					}))}
					onChange={handleLayerTypeChange}
				/>
			</label>
			{markersOptions?.length > 0 && (
				<label style={{ display: 'block', marginTop: '10px' }}>
					<strong>{__('Markers:', 'wpify-mapy-cz')}</strong>
					<Select
						value={selectedMarkers}
						options={markersOptions}
						onChange={handleMarkersChange}
						isMulti
					/>
				</label>
			)}
			{applyFilters('wpify_mapy_cz_map_after_markers', null, { value, onChange, mapycz })}
			{markers.length > 0 && (
				<label style={{ display: 'block', marginTop: '10px' }}>
					<input
						type="checkbox"
						checked={auto_center_zoom}
						onChange={handleCenterZoomByMarkers}
					/>
					{__('Set zoom and center by markers', 'wpify-mapy-cz')}
				</label>
			)}
			<label style={{ display: 'block', marginTop: '10px' }}>
				<input
					type="checkbox"
					checked={show_info_window}
					onChange={handleShowInfoWindow}
				/>
				{__('Show info window', 'wpify-mapy-cz')}
			</label>
			<div style={{ marginTop: '10px' }}>
				<TextControl
					label={<strong>{__('Width:', 'wpify-mapy-cz')}</strong>}
					value={width}
					onChange={handleWidthChange}
					onFocus={() => setCurrentField('width')}
					onBlur={handleSizeBlur}
				/>
				<TextControl
					label={<strong>{__('Height:', 'wpify-mapy-cz')}</strong>}
					value={height}
					onChange={handleHeightChange}
					onFocus={() => setCurrentField('height')}
					onBlur={handleSizeBlur}
				/>
			</div>
			<label style={{ display: 'block', marginTop: '10px' }}>
				<input
					type="checkbox"
					checked={disable_scroll}
					onChange={handleDisableScroll}
				/>
				{__('Disable zoom on scroll', 'wpify-mapy-cz')}
			</label>
		</React.Fragment>
	);
};

addFilter('wpifycf_field_mapycz_map', 'wpify-mapy-cz', Component => MapField);
