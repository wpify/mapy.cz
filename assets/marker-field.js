import React, { useState, useCallback } from 'react';
import { addFilter, applyFilters } from '@wordpress/hooks';
import { __, sprintf } from '@wordpress/i18n';
import { MapContainer, AutoComplete, redIcon } from './components/MapyCz';

const MarkerField = ({ id, value = {}, onChange }) => {
	const [mapycz, setMapycz] = useState();
	const apiKey = 'OMWTptTb3mZbylXy3hFFB7EeC9tmf3bhqlFKqsCyuB4';

	const {
		description = '',
		latitude = 0,
		longitude = 0,
		zoom = 12,
	} = value;

	const handleMarkerDrag = useCallback((event) => {
		const position = event.target.getLatLng();
		onChange({ ...value, longitude: position.lng, latitude: position.lat });
		mapycz.setView(position);
	}, [mapycz, value, onChange]);

	const handleMoveEnd = useCallback((event) => {
		const center = event.target.getCenter();
		onChange({ ...value, latitude: center.lat, longitude: center.lng, zoom: event.target.getZoom() });
	}, [value, onChange]);

	const handleSuggest = useCallback((suggestion) => {
		onChange({ ...value, latitude: suggestion.position.lat, longitude: suggestion.position.lon, address: suggestion.name });
		mapycz.setView(suggestion.position);
	}, [mapycz, value, onChange]);

	return (
		<React.Fragment>
			<div style={{ marginBottom: '1rem' }}>
				<AutoComplete
					apiKey={apiKey}
					onSuggest={handleSuggest}
				/>
			</div>
			{latitude && longitude && (
				<div style={{ marginBottom: '1rem' }}>
					<small>{sprintf(__('latitude: %s, longitude: %s', 'wpify-mapy-cz'), latitude, longitude)}</small>
				</div>
			)}
			<MapContainer
				style={{ height: '400px', marginBottom: '1rem' }}
				className="mapycz"
				map={mapycz}
				setMap={setMapycz}
				center={{ lat: latitude, lng: longitude }}
				zoom={zoom}
				scrollWheelZoom={false}
				onMoveEnd={handleMoveEnd}
				markers={[{ latitude, longitude, icon: redIcon, onDragEnd: handleMarkerDrag, draggable: true }]}
			/>
			{applyFilters('wpify_mapy_cz_marker_form_after_map', null, { value, onChange, mapycz })}
			<div style={{ marginBottom: '1rem' }}>
				<label>
					{__('Description:', 'wpify-mapy-cz')}
					<br/>
					<textarea
						value={description}
						onChange={(e) => onChange({ ...value, description: e.target.value })}
						style={{ width: '100%' }}
						rows={5}
					/>
				</label>
			</div>
		</React.Fragment>
	);
};

addFilter('wpifycf_field_mapycz_marker', 'wpify-mapy-cz', () => MarkerField);
