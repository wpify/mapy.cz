import React, { useEffect, useState } from 'react';
import { doAction } from '@wordpress/hooks';
import { MapContainer } from './components/MapyCz';
import { createRoot } from 'react-dom/client';

const Map = (props) => {
	const [mapycz, setMapycz] = useState();

	useEffect(() => {
		if (mapycz) {
			doAction('wpify_mapy_cz_setup', mapycz, props);
		}
	}, [mapycz, props]);

	return (
		<>
			<MapContainer
				style={{ width: props.width || '100%', height: props.height || '400px', marginBottom: '1rem' }}
				className="smap"
				map={mapycz}
				setMap={setMapycz}
				center={{ lat: props.latitude, lng: props.longitude }}
				zoom={props.zoom}
				mapset={props.layer_type}
				scrollWheelZoom={!props.disable_scroll}
				markers={props.markers}
				cluster={props.cluster_markers}
			/>
		</>
	);
};

document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('[data-mapycz]').forEach(container => {
		const data = window.wpify_mapy_cz[parseInt(container.dataset.mapycz, 10)];
		createRoot(container).render(<Map {...data} element={container}/>);
	});
});
