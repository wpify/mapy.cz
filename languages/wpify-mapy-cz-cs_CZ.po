msgid ""
msgstr ""
"Project-Id-Version: Mapy.cz 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpify-mapy-cz\n"
"POT-Creation-Date: 2022-07-15T15:43:16+02:00\n"
"PO-Revision-Date: 2022-07-15 15:46+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: cs_CZ\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Domain: mapy-cz\n"

#. Plugin Name of the plugin
#: src/Admin.php:139 assets/wpify-mapy-cz-block.js:74
#: build/wpify-mapy-cz-block.js:1
msgid "WPify Mapy.cz"
msgstr "WPify Mapy.cz"

#. Description of the plugin
msgid "Easily embed free mapy.cz maps anywhere"
msgstr ""
"Přidávejte snadno a zdarma Mapy.cz do vašich článků, stránek nebo jiných "
"vlastních post typů"

#. Author of the plugin
msgid "WPify"
msgstr "WPify"

#. Author URI of the plugin
msgid "https://www.wpify.io/"
msgstr "https://www.wpify.io/"

#: src/Admin.php:92 src/Admin.php:93
msgid "Mapy.cz"
msgstr "Mapy.cz"

#: src/Admin.php:102 src/Admin.php:103
msgid "Maps"
msgstr "Mapy"

#: src/Admin.php:110 src/Admin.php:111
msgid "Markers"
msgstr "Značky"

#: src/Admin.php:141
msgid ""
"Welcome! Plugin WPify Mapy.cz by <a href=\"%s\" target=\"_blank\">wpify.io</"
"a> lets you add mapy.cz maps to your site easily!"
msgstr ""
"Vítejte! Plugin WPify Mapy.cz od <a href=\"%s\" target=\"_blank\">wpify.io</"
"a> vám umožní snadno přidávat mapy od Mapy.cz na vaše stránky!"

#: src/Admin.php:148
msgid ""
"You can start adding <a href=\"%s\">maps</a> and <a href=\"%s\">markers</a> "
"right away."
msgstr ""
"Můžete rovnou začít přidávat <a href=\"%s\">mapy</a> a <a href=\"%s"
"\">značky</a>."

#: src/Admin.php:155
msgid "How to create map and insert it to your page"
msgstr "Jak vytvořit mapy a vložit je na vaší stránku"

#: src/Admin.php:158
msgid ""
"Add as many <a href=\"%s\">markers</a> as needed, select it's positions and "
"publish them."
msgstr ""
"Přidejte <a href=\"%s\">značek</a> kolik chcete, vyberte jejich pozici a "
"publikujte je."

#: src/Admin.php:163
msgid ""
"Add a new <a href=\"%s\">map</a>, enter the options, choose markers to "
"display and click Publish."
msgstr ""
"Přidejte novou <a href=\"%s\">mapu</a>, přizpůsobte ji, vyberte značky k "
"zobrazení a publikujte svou mapu."

#: src/Admin.php:171
msgid ""
"To insert the map into your page, you can either use <i>Mapy.cz</i> "
"Gutenberg block, or you can use the shortcode provided in the maps <a href="
"\"%s\">admin listing</a>"
msgstr ""
"Pro přidání mapy na stránku můžete využít jak <i>Mapy.cz</i> gutenberg "
"block, nebo můžete použít shortcode, který naleznete v <a href=\"%s"
"\">seznamu map</a>"

#: src/Admin.php:183
msgid "Shortcode"
msgstr "Shortcode"

#: src/Admin.php:198
msgid "Settings"
msgstr "Nastavení"

#: src/Admin.php:202
msgid "Get more plugins and support"
msgstr "Získejte další pluginy a podporu"

#: src/Managers/PostTypesManager.php:29
msgid "Marker details"
msgstr "Detaily značky"

#: src/Managers/PostTypesManager.php:37
msgid "Marker"
msgstr "Značka"

#: src/PostTypes/MapPostType.php:23
msgid "Map details"
msgstr "Detail mapy"

#: src/PostTypes/MapPostType.php:31
msgid "Map"
msgstr "Mapa"

#: src/PostTypes/MapPostType.php:44
msgctxt "post type general name"
msgid "Maps"
msgstr "Mapy"

#: src/PostTypes/MapPostType.php:45
msgctxt "post type singular name"
msgid "Map"
msgstr "Mapa"

#: src/PostTypes/MapPostType.php:46
msgctxt "admin menu"
msgid "Maps"
msgstr "Mapy"

#: src/PostTypes/MapPostType.php:47
msgctxt "add new on admin bar"
msgid "Map"
msgstr "Mapa"

#: src/PostTypes/MapPostType.php:49
msgid "Add New Map"
msgstr "Přidat novou mapu"

#: src/PostTypes/MapPostType.php:50
msgid "New Map"
msgstr "Nová mapa"

#: src/PostTypes/MapPostType.php:51
msgid "Edit Map"
msgstr "Upravit mapu"

#: src/PostTypes/MapPostType.php:52
msgid "View Map"
msgstr "Zobrazit mapu"

#: src/PostTypes/MapPostType.php:53
msgid "All Maps"
msgstr "Všechny mapy"

#: src/PostTypes/MapPostType.php:54
msgid "Search Maps"
msgstr "Vyhledat mapu"

#: src/PostTypes/MapPostType.php:55
msgid "Parent Maps:"
msgstr "Nadřízená mapa:"

#: src/PostTypes/MapPostType.php:56
msgid "No Maps found."
msgstr "Žádná mapa nenalezena."

#: src/PostTypes/MapPostType.php:57
msgid "No Maps found in Trash."
msgstr "V koši není žádná mapa."

#: src/PostTypes/MapPostType.php:59
msgid "Description of Map."
msgstr "Popis mapy."

#: src/PostTypes/MapPostType.php:66
msgid "map"
msgstr "mapa"

#: src/PostTypes/MarkerPostType.php:18
msgctxt "post type general name"
msgid "Markers"
msgstr "Značky"

#: src/PostTypes/MarkerPostType.php:19
msgctxt "post type singular name"
msgid "Marker"
msgstr "Značky"

#: src/PostTypes/MarkerPostType.php:20
msgctxt "admin menu"
msgid "Markers"
msgstr "Značky"

#: src/PostTypes/MarkerPostType.php:21
msgctxt "add new on admin bar"
msgid "Marker"
msgstr "Značka"

#: src/PostTypes/MarkerPostType.php:23
msgid "Add New Marker"
msgstr "Přidat novou značku"

#: src/PostTypes/MarkerPostType.php:24
msgid "New Marker"
msgstr "Nová značka"

#: src/PostTypes/MarkerPostType.php:25
msgid "Edit Marker"
msgstr "Upravit značku"

#: src/PostTypes/MarkerPostType.php:26
msgid "View Marker"
msgstr "Zobrazit značku"

#: src/PostTypes/MarkerPostType.php:27
msgid "All Markers"
msgstr "Všechny značky"

#: src/PostTypes/MarkerPostType.php:28
msgid "Search Markers"
msgstr "Vyhledat značky"

#: src/PostTypes/MarkerPostType.php:29
msgid "Parent Markers:"
msgstr "Nadřízená značka:"

#: src/PostTypes/MarkerPostType.php:30
msgid "No Markers found."
msgstr "Žádná značka nebyla nalezena."

#: src/PostTypes/MarkerPostType.php:31
msgid "No Markers found in Trash."
msgstr "Žádná značka nebyla nalezena v koši."

#: src/PostTypes/MarkerPostType.php:33
msgid "Description of Marker."
msgstr "Popis značky."

#: src/PostTypes/MarkerPostType.php:40
msgid "marker"
msgstr "značka"

#: wpify-mapy-cz.php:65
msgid ""
"<div class=\"error notice\"><p>Opps! %s requires a minimum PHP version of "
"%s. Your current version is: %s. Please contact your host to upgrade.</p></"
"div>"
msgstr ""
"<div class=\"error notice\"><p>Sakryš! %s vyžaduje minimální verzi PHP %s. "
"Vaše aktuální verze je: %s. Kontaktujte prosím svého poskytovatele "
"webhostingu a požádejte ho o aktualizaci.</p></div>"

#: wpify-mapy-cz.php:76
msgid ""
"<div class=\"error notice\"><p>Opps! %s is corrupted it seems, please re-"
"install the plugin.</p></div>"
msgstr ""
"<div class=\"error notice\"><p>Sakryš! Zdá se, že plugin %s je poškozen, "
"nainstalujte prosím plugin znovu.</p></div>"

#: assets/map-field.js:17 build/map-field.js:1
msgid "Basic map"
msgstr "Základní mapa"

#: assets/map-field.js:18 build/map-field.js:1
msgid "Ortho map"
msgstr "Ortho mapa"

#: assets/map-field.js:19 build/map-field.js:1
msgid "Hybrid map"
msgstr "Hybridní mapa"

#: assets/map-field.js:20 build/map-field.js:1
msgid "Turist map"
msgstr "Turistická mapa"

#: assets/map-field.js:21 build/map-field.js:1
msgid "Winter turist map"
msgstr "Zimní turistická mapa"

#: assets/map-field.js:136 assets/marker-field.js:112 build/map-field.js:1
#: build/marker-field.js:1
msgid "Description:"
msgstr "Popis značky:"

#: assets/map-field.js:150 build/map-field.js:1
msgid "Display type:"
msgstr "Typ zobrazení:"

#: assets/map-field.js:167 build/map-field.js:1
msgid "Markers:"
msgstr "Značky:"

#: assets/map-field.js:184 build/map-field.js:1
msgid "Set zoom and center by markers"
msgstr "Automaticky nastavit zoom a střed podle zobrazených markerů"

#: assets/map-field.js:194 build/map-field.js:1
msgid "Show info window"
msgstr "Zobrazit vizitku"

#: assets/map-field.js:199 build/map-field.js:1
msgid "Width:"
msgstr "Šířka:"

#: assets/map-field.js:204 build/map-field.js:1
msgid "Height:"
msgstr "Výška:"

#: assets/marker-field.js:99 build/marker-field.js:1
msgid "Type to search..."
msgstr "Zadejte hledaný výraz..."

#: assets/marker-field.js:104 build/marker-field.js:1
msgid "latitude: %s, longitude: %s"
msgstr "zeměpisná šířka: %s, zeměpisná délka: %s"

#: assets/wpify-mapy-cz-block.js:53 build/wpify-mapy-cz-block.js:1
msgid "Select map"
msgstr "Vyberte mapu"

#: assets/wpify-mapy-cz-block.js:63 build/wpify-mapy-cz-block.js:1
msgid "WPify Mapy.cz:"
msgstr "WPify Mapy.cz:"

#~ msgid "Please add id param to the shortcode"
#~ msgstr "Prosím zadejte id parametr do shortcode"

#~ msgid "Please provide valid ID"
#~ msgstr "Prosím zadejte platné ID"

#~ msgid "Search:"
#~ msgstr "Vyhledat:"

#~ msgid "Description..."
#~ msgstr "Popis značky…"

#~ msgid "WPify Map Editor"
#~ msgstr "Editor mapy"

#~ msgid "Editor for WPify Mapy.cz!"
#~ msgstr "Editor pro WPify Mapy.cz!"

#~ msgid "Mapy.cz:"
#~ msgstr "Mapy.cz:"

#~ msgid "Enter map width. Include <i>px</i> or <i>%</i>"
#~ msgstr "Zadejte šířku mapy v <i>px</i> nebo v <i>%</i>"

#~ msgid "Enter map height. Include <i>px</i> or <i>%</i>"
#~ msgstr "Zadejte výšku mapy v <i>px</i> nebo v <i>%</i>"

#~ msgid "Check to show Info Window"
#~ msgstr "Zaškrtněte pro zobrazení vizitky"
