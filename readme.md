# WPify Mapy.cz

Easily embed maps from [mapy.cz](https://mapy.cz) in your posts, pages and custom post types.

### Features

The free version of the plugin includes many amazing features:

* unlimited maps
* unlimited markers
* set basic map options
    - width / height
    - center
    - zoom
    - map type - Basic, Photo, Tourist etc.
    - select markers to display
    - show/hide Marker Info Window
* use provided shortcode or Gutenberg block to add maps to posts / pages / Custom Post Types

### Pro version

Pro version is currently in the works and will be available soon with advanced features:

* custom images for markers
* markers clustering
* maps and markers categories
* show / hide map filters by markers category
* more map options
* add search geolocation input with autocomplete to the map
* add markers to your Custom Post types to easily build filterable listings
* custom Info Window content
* ability to use GPX files

This plugin is brought to you by [wpify.io](https://wpify.io).
