<?php

namespace WpifyMapyCz;

use WpifyMapyCz\Managers\ApiManager;
use WpifyMapyCz\PostTypes\MapPostType;
use WpifyMapyCz\PostTypes\MarkerPostType;
use WpifyMapyCzDeps\Wpify\Asset\AssetFactory;
use WpifyMapyCzDeps\Wpify\CustomFields\CustomFields;
use WpifyMapyCzDeps\Wpify\CustomFields\Integrations\Options;
use WpifyMapyCzDeps\Wpify\PluginUtils\PluginUtils;

class Admin {
	public function __construct(
		private readonly PluginUtils $plugin_utils,
		private readonly AssetFactory $asset_factory,
		private readonly CustomFields $custom_fields,
	) {
		if ( is_admin() ) {
			$this->setup();
		}
	}

	/**
	 * Register hooks
	 * @return bool|void
	 */
	public function setup() {
		add_action( 'admin_menu', array( $this, 'add_menu_pages' ), 11 );
		add_filter( 'manage_' . MapPostType::KEY . '_posts_columns', array( $this, 'add_admin_columns' ) );
		add_action( 'manage_' . MapPostType::KEY . '_posts_custom_column', array( $this, 'admin_columns_content' ), 10, 2 );
		add_filter( 'plugin_action_links_wpify-mapy-cz/wpify-mapy-cz.php', array( $this, 'add_action_links' ) );
		add_action( 'init', array( $this, 'enqueue' ), 0 );

		add_filter( 'wpifycf_sanitize_mapycz_marker', array( $this, 'sanitize_marker' ), 10, 3 );
		add_filter( 'wpifycf_wp_type_mapycz_marker', array( $this, 'return_object' ) );
		add_filter( 'wpifycf_default_value_mapycz_marker', array( $this, 'marker_default_value' ) );

		add_filter( 'wpifycf_sanitize_mapycz_map', array( $this, 'sanitize_map' ), 10, 3 );
		add_filter( 'wpifycf_wp_type_mapycz_map', array( $this, 'return_object' ) );
		add_filter( 'wpifycf_default_value_mapycz_map', array( $this, 'map_default_value' ) );

		add_action( 'admin_notices', array( $this, 'notice_api_key_missing' ) );

		$this->custom_fields->create_options_page(
			array(
				'type'       => Options::TYPE_OPTIONS,
				'page_title' => __( 'WPify Mapy.cz', 'wpify-mapy-cz' ),
				'menu_title' => __( 'Mapy.cz', 'wpify-mapy-cz' ),
				'capability' => apply_filters( 'mapy_cz_menu_capability', 'manage_options' ),
				'menu_slug'  => 'wpify-mapy-cz',
				'position'   => 30,
				'callback'   => array( $this, 'render_main_menu_page' ),
				'icon_url'   => 'data:image/svg+xml;base64,' . base64_encode( file_get_contents( $this->plugin_utils->get_plugin_path( 'assets/images/mapy-cz.svg' ) ) ),
				'items'      => array(
					'mapy_cz_api_key' => array(
						'type'        => 'text',
						'label'       => __( 'Mapy.cz API key', 'wpify-mapy-cz' ),
						'description' => __( 'You can create your API key for free at <a href="https://developer.mapy.cz/account/projects" target="_blank">https://developer.mapy.cz/account/projects</a>', 'wpify-mapy-cz' ),
					),
				),
			)
		);
	}

	public function enqueue() {
		$this->asset_factory->admin_wp_script( $this->plugin_utils->get_plugin_path( 'build/marker-field.js' ) );
		$this->asset_factory->admin_wp_script(
			$this->plugin_utils->get_plugin_path( 'build/map-field.js' ),
			array(
				'variables' => array(
					'wpify_mapy_cz' => array(
						'markers_api' => ApiManager::NAMESPACE . '/markers',
						'api_key'     => get_option( 'mapy_cz_api_key' ),
					),
				),
			)
		);
	}

	/**
	 * Add admin menu pages
	 */
	public function add_menu_pages() {
		add_submenu_page(
			'wpify-mapy-cz',
			__( 'Maps', 'wpify-mapy-cz' ),
			__( 'Maps', 'wpify-mapy-cz' ),
			apply_filters( 'mapy_cz_menu_capability', 'manage_options' ),
			$this->get_admin_url_maps(),
			null
		);

		add_submenu_page(
			'wpify-mapy-cz',
			__( 'Markers', 'wpify-mapy-cz' ),
			__( 'Markers', 'wpify-mapy-cz' ),
			apply_filters( 'mapy_cz_menu_capability', 'manage_options' ),
			$this->get_admin_url_markers(),
			null
		);
	}

	/**
	 * Get admin URL for maps
	 * @return string
	 */
	public function get_admin_url_maps() {
		return sprintf( 'edit.php?post_type=%s', MapPostType::KEY );
	}

	/**
	 * Get admin URL for markers
	 * @return string
	 */
	public function get_admin_url_markers() {
		return sprintf( 'edit.php?post_type=%s', MarkerPostType::KEY );
	}

	/**
	 * Render main admin menu page
	 */
	public function render_main_menu_page() { ?>
			<p><?php printf(
					__(
						'Welcome! Plugin WPify Mapy.cz by <a href="%s" target="_blank">wpify.io</a> lets you add mapy.cz maps to your site easily!',
						'wpify-mapy-cz'
					),
					'https://wpify.io'
				) ?></p>
			<p><?php printf(
					__(
						'You can start adding <a href="%s">maps</a> and <a href="%s">markers</a> right away.',
						'wpify-mapy-cz'
					),
					$this->get_admin_url_maps(),
					$this->get_admin_url_markers()
				) ?></p>
			<h3><?php _e( 'How to create map and insert it to your page', 'wpify-mapy-cz' ); ?></h3>
			<ol>
				<li><?php printf(
						__( 'Create a <a href="%s" target="_blank">Mapy.cz API key</a> and save it bellow.', 'wpify-mapy-cz' ),
						'https://developer.mapy.cz/account/projects'
					) ?>
				</li>
				<li><?php printf(
						__( 'Add as many <a href="%s">markers</a> as needed, select it\'s positions and publish them.', 'wpify-mapy-cz' ),
						$this->get_admin_url_markers()
					) ?>
				</li>
				<li><?php printf(
						__(
							'Add a new <a href="%s">map</a>, enter the options, choose markers to display and click Publish.',
							'wpify-mapy-cz'
						),
						$this->get_admin_url_maps()
					) ?>
				</li>
				<li><?php printf(
						__(
							'To insert the map into your page, you can either use <i>Mapy.cz</i> Gutenberg block, or you can use the shortcode provided in the maps <a href="%s">admin listing</a>',
							'wpify-mapy-cz'
						),
						$this->get_admin_url_maps()
					) ?>
				</li>
			</ol>
	<?php }

	public function add_admin_columns( $columns ) {
		$columns['shortcode'] = __( 'Shortcode', 'wpify-mapy-cz' );

		return $columns;
	}

	public function admin_columns_content( $column_name, $post_id ) {
		switch ( $column_name ) {
			case 'shortcode':
				printf( '[wpify_mapy_cz id="%s"]', $post_id );
				break;
		}
	}

	public function add_action_links( $links ) {
		$before = array(
			'settings' => sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=wpify-mapy-cz' ), __( 'Settings', 'wpify-mapy-cz' ) ),
		);

		$after = array(
			'wpify' => sprintf( '<a href="%s" target="_blank">%s</a>', 'https://wpify.io', __( 'Get more plugins and support', 'wpify-mapy-cz' ) ),
		);

		return array_merge( $before, $links, $after );
	}

	public function sanitize_marker( $sanitized_value, $original_value, $item ): array {
		$value                          = is_string( $original_value ) ? json_decode( $original_value, true ) : (array) $original_value;
		$sanitized_value                = array();
		$sanitized_value['latitude']    = floatval( $value['latitude'] ?? 0 );
		$sanitized_value['longitude']   = floatval( $value['longitude'] ?? 0 );
		$sanitized_value['zoom']        = intval( $value['zoom'] ?? 0 );
		$sanitized_value['address']     = sanitize_text_field( $value['address'] ?? '' );
		$sanitized_value['description'] = sanitize_textarea_field( $value['description'] ?? '' );

		return $sanitized_value;
	}

	public function sanitize_map( $sanitized_value, $original_value, $item ): array {
		$value                               = is_string( $original_value ) ? json_decode( $original_value, true ) : (array) $original_value;
		$sanitized_value                     = array();
		$sanitized_value['latitude']         = floatval( $value['latitude'] ?? 0 );
		$sanitized_value['longitude']        = floatval( $value['longitude'] ?? 0 );
		$sanitized_value['zoom']             = intval( $value['zoom'] ?? 0 );
		$sanitized_value['layer_type']       = sanitize_text_field( $value['layer_type'] ?? '' );
		$sanitized_value['description']      = sanitize_textarea_field( $value['description'] ?? '' );
		$sanitized_value['width']            = sanitize_text_field( $value['width'] ?? '' );
		$sanitized_value['height']           = sanitize_text_field( $value['height'] ?? '' );
		$sanitized_value['auto_center_zoom'] = filter_var( $value['auto_center_zoom'] ?? '', FILTER_VALIDATE_BOOLEAN );
		$sanitized_value['show_info_window'] = filter_var( $value['show_info_window'] ?? '', FILTER_VALIDATE_BOOLEAN );
		$sanitized_value['disable_scroll']   = filter_var( $value['disable_scroll'] ?? '', FILTER_VALIDATE_BOOLEAN );
		$sanitized_value['markers']          = is_array( $value['markers'] )
			? array_map( 'intval', $value['markers'] ?? array() )
			: array();

		return $sanitized_value;
	}

	public function return_object(): string {
		return 'object';
	}

	public function marker_default_value(): array {
		return array(
			'latitude'  => 0,
			'longitude' => 0,
			'zoom'      => 12,
			'address'   => '',
		);
	}

	public function map_default_value(): array {
		return array(
			'latitude'  => 0,
			'longitude' => 0,
			'zoom'      => 12,
			'address'   => '',
		);
	}

	public function notice_api_key_missing() {
		if ( ! get_option( 'mapy_cz_api_key' ) ) {
			printf(
				'<div class="notice notice-error"><p>' . __( 'Mapy.cz API key is missing, please add it in the <a href="%s">settings</a>.', 'wpify-mapy-cz' ) . '</p></div>',
				admin_url( 'admin.php?page=wpify-mapy-cz' )
			);
		}
	}
}
