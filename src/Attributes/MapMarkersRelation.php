<?php

namespace WpifyMapyCz\Attributes;

use Attribute;
use WpifyMapyCz\Models\MarkerModel;
use WpifyMapyCzDeps\Wpify\Model\Interfaces\ModelInterface;
use WpifyMapyCzDeps\Wpify\Model\Interfaces\SourceAttributeInterface;

#[Attribute( Attribute::TARGET_PROPERTY )]
class MapMarkersRelation implements SourceAttributeInterface {
	public function get( ModelInterface $model, ?string $key = null ): mixed {
		$manager           = $model->manager();
		$target_repository = $manager->get_model_repository( MarkerModel::class );

		return $target_repository->find( array( 'post__in' => $model->_mapy_cz_data['markers'] ) );
	}
}
