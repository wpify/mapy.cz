<?php

namespace WpifyMapyCz\Managers;

use WpifyMapyCz\Repositories\MapRepository;
use WpifyMapyCz\Repositories\MarkerRepository;
use WpifyMapyCzDeps\DI\Container;
use WpifyMapyCzDeps\Wpify\Model\Manager;

class RepositoriesManager {
	public function __construct(
		Manager $manager,
		Container $container,
		MapRepository $map_repository,
		MarkerRepository $marker_repository,
	) {
		foreach ( $manager->get_repositories() as $repository ) {
			$container->set( $repository::class, $repository );
		}

		$manager->register_repository( $map_repository );
		$manager->register_repository( $marker_repository );

	}
}
