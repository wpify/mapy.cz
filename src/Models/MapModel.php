<?php

namespace WpifyMapyCz\Models;

use WpifyMapyCz\Attributes\MapMarkersRelation;
use WpifyMapyCz\Repositories\MapRepository;
use WpifyMapyCzDeps\Wpify\Model\Attributes\Meta;
use WpifyMapyCzDeps\Wpify\Model\Post;

/**
 * @method MapRepository model_repository()
 */
class MapModel extends Post {
	#[Meta]
	public array $_mapy_cz_data = array();

	public bool $auto_center_zoom = false;

	public string $description = '';

	public string $width = '';

	public string $height = '';

	public float $latitude = 0;

	public float $longitude = 0;

	public bool $show_info_window = false;

	public int $zoom = 11;

	#[MapMarkersRelation]
	public array $markers = array();

	public string $layer_type = 'basic';

	public function get_auto_center_zoom(): bool {
		return $this->_mapy_cz_data['auto_center_zoom'] ?? false;
	}

	public function get_description(): string {
		return $this->_mapy_cz_data['description'] ?? '';
	}

	public function get_width(): string {
		return $this->_mapy_cz_data['width'] ?? '100%';
	}

	public function get_height(): string {
		return $this->_mapy_cz_data['height'] ?? '400px';
	}

	public function get_latitude(): float {
		return $this->_mapy_cz_data['latitude'] ?? 0;
	}

	public function get_longitude(): float {
		return $this->_mapy_cz_data['longitude'] ?? 0;
	}

	public function get_show_info_window(): bool {
		return $this->_mapy_cz_data['show_info_window'] ?? false;
	}

	public function get_zoom(): int {
		return $this->_mapy_cz_data['zoom'] ?? 1;
	}

	public function get_layer_type(): string {
		return $this->_mapy_cz_data['layer_type'] ?? 'DEF_BASE';
	}
}
