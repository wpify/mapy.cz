<?php

namespace WpifyMapyCz\Models;

use WpifyMapyCz\Repositories\MarkerRepository;
use WpifyMapyCzDeps\Wpify\Model\Attributes\Meta;
use WpifyMapyCzDeps\Wpify\Model\Post;

/**
 * @method MarkerRepository model_repository()
 */
class MarkerModel extends Post {
	#[Meta]
	public array $_mapy_cz_data = array();

	public string $address = '';

	public string $description = '';

	public float $latitude = 0;

	public float $longitude = 0;

	public int $zoom = 11;

	public function get_address(): string {
		return $this->_mapy_cz_data['address'] ?? '';
	}

	public function get_description(): string {
		return $this->_mapy_cz_data['description'] ?? '';
	}

	public function get_latitude(): float {
		return $this->_mapy_cz_data['latitude'] ?? 0;
	}

	public function get_longitude(): float {
		return $this->_mapy_cz_data['longitude'] ?? 0;
	}

	public function get_zoom(): int {
		return $this->_mapy_cz_data['zoom'] ?? 0;
	}
}
