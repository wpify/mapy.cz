<?php

namespace WpifyMapyCz\Repositories;

use WpifyMapyCz\Models\MapModel;
use WpifyMapyCz\Models\MarkerModel;
use WpifyMapyCz\PostTypes\MapPostType;
use WpifyMapyCzDeps\Wpify\Model\PostRepository;

/**
 * @method MarkerModel get( $object = null )
 */
class MapRepository extends PostRepository {
	public function __construct(
		private readonly MarkerRepository $marker_repository
	) {
	}

	public function post_types(): array {
		return array( MapPostType::KEY );
	}

	public function model(): string {
		return MapModel::class;
	}

	public function get_marker_repository(): MarkerRepository {
		return $this->marker_repository;
	}
}
