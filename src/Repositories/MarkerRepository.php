<?php

namespace WpifyMapyCz\Repositories;

use WpifyMapyCz\Models\MarkerModel;
use WpifyMapyCz\PostTypes\MarkerPostType;
use WpifyMapyCzDeps\Wpify\Model\PostRepository;

/**
 * @method MarkerModel get( $object = null )
 */
class MarkerRepository extends PostRepository {
	public function post_types(): array {
		return array( MarkerPostType::KEY );
	}

	public function model(): string {
		return MarkerModel::class;
	}
}
