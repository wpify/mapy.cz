const path = require('path');
const fs = require('fs');
const globImporter = require('node-sass-glob-importer');
const defaultConfig = require('@wordpress/scripts/config/webpack.config');

module.exports = {
  ...defaultConfig,
  entry: {
		'plugin': ['./assets/plugin.js', './assets/plugin.scss'],
		'marker-field': './assets/marker-field.js',
		'map-field': './assets/map-field.js',
		'wpify-mapy-cz-block': './assets/wpify-mapy-cz-block.js',
	},
  module: {
    ...defaultConfig.module,
    rules: [
      ...defaultConfig.module.rules.map((rule) => {
        if (rule.test.test('.scss')) {
          rule.use.forEach(use => {
            if (use.loader === require.resolve('sass-loader')) {
              use.options.sassOptions = {
                ...(use.options.sassOptions || null),
                importer: globImporter(),
              };
            }
          });
        }

        return rule;
      }),
    ],
  },
};
